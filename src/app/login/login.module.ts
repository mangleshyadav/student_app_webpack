import { LoginRoutingModule } from './login.routing.module';

import { LoginComponent } from './login.component';
import { NgModule, Component } from '@angular/core';
import { FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [LoginRoutingModule, ReactiveFormsModule,
    FormsModule],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  providers: []
})
export class LoginModule { }

