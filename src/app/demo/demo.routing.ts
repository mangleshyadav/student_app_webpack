import { HelloComponent } from './hello.component';
import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'demo', component: HelloComponent }
  ])],
  exports: [RouterModule]
})
export class DemodRoutingModule {}
