import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule }            from '@angular/core';
import { DataTableModule } from "angular2-datatable";
import { SharedModule }        from '../shared/shared.module';

import { StudentComponent }       from './student.component';
import { StudentDetailComponent } from './student-detail.component';
import { StudentListComponent }   from './student-list.component';
import { StudentRoutingModule }   from './student-routing.module';

@NgModule({
  imports: [ SharedModule, StudentRoutingModule, DataTableModule, FormsModule, ReactiveFormsModule ],
  declarations: [
    StudentComponent, StudentDetailComponent, StudentListComponent,
  ]
})
export class StudentModule { }
